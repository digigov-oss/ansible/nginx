# Ansible nginx role

## Overview

This is an Ansible role for installing nginx on Debian or Ubuntu. It
merely installs the nginx package and performs some essential
configuration such as allowing ports 80 and 443 through the firewall.
Other than that, it doesn't do much and should be combined with
`nginx-domain`.

## Options

- `use_ferm`: If `true`, it drops a configuration snippet in
  `/etc/ferm/ansible-late` in order to allow connections to ports 80
  and 443.  In this case you must also use
  [common](https://github.com/aptiko-ansible/common). The default is
  `false` for backwards compatibility reasons.

## Meta

Written by Antonis Christofides

Copyright (C) 2022 GRNET  
Copyright (C) 2011-2015 Antonis Christofides  
Copyright (C) 2014-2021 TEI of Epirus  
Copyright (C) 2015-2021 National Technical University of Athens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
